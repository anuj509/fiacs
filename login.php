<?php
include 'database.php';
session_start();
if(isset($_SESSION['email']) && !empty($_SESSION['email']))
{
	header('Location:facultyform.php');
}
if(isset($_SESSION['adminemail']) && !empty($_SESSION['adminemail']))
{
	header('Location:index.php');
}
if(isset($_POST['login']))
{
	if(!empty($_POST['email']) && !empty($_POST['pass']))
	{
		//print_r($_POST);
		$email=mysql_real_escape_string($_POST['email']);
		$pass1=mysql_real_escape_string($_POST['pass']);
		$pass=md5($pass1);
		$checkadmin=mysql_query("SELECT * FROM `adminaccount` WHERE `username`='$email'");
		if(mysql_num_rows($checkadmin)==0){
		$res=mysql_query("SELECT * FROM `users` WHERE `email`='$email'") or die(mysql_error());
		if(mysql_num_rows($res)>0)
		{
		$row=mysql_fetch_array($res);
		$dbpass=$row['password'];
			if(strcmp($dbpass,$pass)==0)	
			{
				echo strcmp($dbpass,$pass);
				$_SESSION['email']=$row['email'];
				header('Location:facultyform.php');
			}	

			else
			{
				//echo '<script language="javascript">';
				echo "Incorrect email or password";
				//echo '</script>';
	
			}
		}
		else
		{
			// '<script language="javascript">';
			echo "User not registered";
			//echo '</script>';
	
		}
	}else{
		
		if(mysql_num_rows($checkadmin)>0)
		{
		$row=mysql_fetch_array($checkadmin);
		$dbpass=$row['password'];
			if(strcmp($dbpass,$pass)==0)	
			{
				//echo strcmp($dbpass,$pass);
				$_SESSION['adminemail']=$row['username'];
				header('Location:index.php');
			}	

			else
			{
				//echo '<script language="javascript">';
				echo "Incorrect email or password";
				//echo '</script>';
	
			}
		}
		else
		{
			// '<script language="javascript">';
			echo "User not registered";
			//echo '</script>';
	
		}	
	}	
	}	
	else
	{
//echo '<script language="javascript">';
		echo "Fields are empty";
		//echo '</script>';
	}

}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>FIACS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Faculty Information And Contribution Management System</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <h6>Sign In</h6>
			                
			                <form method="post">
			          
			                <input class="form-control"  name="email" type="text" placeholder="E-mail address">
			                <input class="form-control" name="pass" type="password" placeholder="Password">
			                <div class="action">
			                    <button class="btn btn-primary signup" name="login" type="submit">Login</button>
			                </div>                
			            </form>
			            </div>
			        </div>

			        <div class="already">
			            <p>Don't have an account yet?</p>
			            <a href="signup.php">Sign Up</a>
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>