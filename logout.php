<?php
session_start();
if(!isset($_SESSION['email']) || !isset($_SESSION['adminemail']))
{
 header("Location: login.php");
}
else if(isset($_SESSION['email'])!="" || isset($_SESSION['adminemail'])!="")
{
 header("Location: login.php");
}

if(isset($_GET['logout']))
{

 session_destroy();
 if(isset($_SESSION['semail']))
 unset($_SESSION['email']);
 if(isset($_SESSION['adminemail']))
 unset($_SESSION['adminemail']);
 header("Location: login.php");
}

?>