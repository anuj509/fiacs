<!DOCTYPE html>
<html>
  <head>
    <title>National Journal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<div class="header">
       <div class="container">
          <div class="row">
             <div class="col-md-10">
                <!-- Logo -->
                <div class="logo">
                   <h1><a href="index.html">Faculty Information And Contribution Management System</a></h1>
                </div>
             </div>
             
             <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                      <ul class="nav navbar-nav">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                          <ul class="dropdown-menu animated fadeInUp">
                            
                            <li><a href="logout.php?logout">Logout</a></li>
                          </ul>
                        </li>
                      </ul>
                    </nav>
                </div>
             </div>
          </div>
       </div>
  </div>
    <div class="page-content">
    	<div class="row">
		  <?php include 'sidenavbar.php'; ?>
		  <div class="col-md-10">

		  	<div class="row">
  				<div class="col-md-12">
  					

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title">International Journal</div>
				</div>
  				<div class="panel-body">
  				
					<div id="live_data"></div>  
                     <div id="response"></div>  
  				</div>
  			</div>



		  </div>
		</div>
    </div>

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2014 <a href='#'>Website</a>
            </div>
            
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>

    <script src="vendors/datatables/dataTables.bootstrap.js"></script>

    <script src="js/custom.js"></script>
    <script src="js/tables.js"></script>
   <!-- <script src="js/tables.js"></script>-->
    <script>  

 $(document).ready(function(){  
      function fetch_data()  
      {  
           $.ajax({  
                url:"inter_select.php",  
                method:"POST",  
                success:function(data){  
                     $('#live_data').html(data);  
                }  
           });  
      }  
      fetch_data();  
      $(document).on('click', '#btn_add', function(){  
           var coauthor = $('#coauthor').text();  
           var researchpaper = $('#researchpaper').text(); 
           var year = $('#year').text();  
           var jou_detail = $('#jou_detail').text();  
           var impactfact = $('#impactfact').text();  
           
           if(coauthor == '')  
           {  
                alert("Enter coauthor");  
                return false;  
           }  
           if(researchpaper == '')  
           {  
                alert("Enter researchpaper");  
                return false;  
           }  
           $.ajax({  
                url:"inter_insert.php",  
                method:"POST",  
                data:{coauthor:coauthor, researchpaper:researchpaper, year:year, jou_detail:jou_detail, impactfact:impactfact},  
                dataType:"text",  
                success:function(data)  
                {  
                     //alert(data);  
                      $("#response").html(data);
                     fetch_data();  
                }  
           })  
      });  
      function edit_data(id, text, column_name)  
      {  
           $.ajax({  
                url:"inter_edit.php",  
                method:"POST",  
                data:{id:id, text:text, column_name:column_name},      
                dataType:"text",  
                success:function(data){  
                    //alert(data);  
                   $("#result").html(data);
                    //fetch_data(); 
                    console.log(data);
                }  
           });  
      }  
      $(document).on('blur', '.coauthor', function(){  
           var id = $(this).data("id1");  
           var coauthor = $(this).text();  
           edit_data(id, coauthor, "coauthor");  
      });  
      $(document).on('blur', '.researchpaper', function(){  
           var id = $(this).data("id2");  
           var researchpaper = $(this).text();  
           edit_data(id,researchpaper, "researchpaper");  
      });
      $(document).on('blur', '.year', function(){  
           var id = $(this).data("id3");  
           var year = $(this).text();  
           edit_data(id,year, "year");  
      });
      $(document).on('blur', '.jou_detail', function(){  
           var id = $(this).data("id4");  
           var jou_detail = $(this).text();  
           edit_data(id,jou_detail, "jou_detail");  
      });
      $(document).on('blur', '.impactfact', function(){  
           var id = $(this).data("id5");  
           var impactfact = $(this).text();  
           edit_data(id,impactfact, "impactfact");  
      });  
      $(document).on('click', '.btn_delete', function(){  
           var id=$(this).data("id3");  
           if(confirm("Are you sure you want to delete this?"))  
           {  
                $.ajax({  
                     url:"inter_delete.php",  
                     method:"POST",  
                     data:{id:id},  
                     dataType:"text",  
                     success:function(data){  
                          alert(data);  
                          fetch_data();  
                     }  
                });  
           }  
      });  
 });  
 </script>  
  </body>
</html>