-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2017 at 05:25 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fiacs`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_details`
--

CREATE TABLE `academic_details` (
  `ID` int(11) NOT NULL,
  `department` varchar(20) NOT NULL,
  `desg` varchar(30) NOT NULL,
  `teaching_exp` int(2) NOT NULL,
  `prof_exp` int(2) NOT NULL,
  `doj_a` varchar(10) NOT NULL,
  `doj_r` varchar(10) NOT NULL,
  `approve_status` varchar(20) NOT NULL,
  `achievement` varchar(150) NOT NULL,
  `publication` varchar(500) NOT NULL,
  `st_be` varchar(500) NOT NULL,
  `st_me` varchar(500) NOT NULL,
  `events_orgs` varchar(500) NOT NULL,
  `projects_guided` varchar(500) NOT NULL,
  `sttp` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_details`
--

INSERT INTO `academic_details` (`ID`, `department`, `desg`, `teaching_exp`, `prof_exp`, `doj_a`, `doj_r`, `approve_status`, `achievement`, `publication`, `st_be`, `st_me`, `events_orgs`, `projects_guided`, `sttp`) VALUES
(39, 'it', 'sss', 0, 0, 'no', 'no', 'no', 'ddd', 'ss', '', '', '', '', ''),
(40, 'it', 'asst Prof', 0, 0, 'no', 'no', 'no', 'None', 'IJTRD', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `adminaccount`
--

CREATE TABLE `adminaccount` (
  `ID` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminaccount`
--

INSERT INTO `adminaccount` (`ID`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `basic_info`
--

CREATE TABLE `basic_info` (
  `PID` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `marital_status` tinyint(1) NOT NULL,
  `DOB` date NOT NULL,
  `age` varchar(4) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `pan_no` varchar(20) NOT NULL,
  `aadhar` varchar(20) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `altemail` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_info`
--

INSERT INTO `basic_info` (`PID`, `email`, `fullname`, `address`, `marital_status`, `DOB`, `age`, `gender`, `pan_no`, `aadhar`, `contact`, `altemail`) VALUES
(1, 'anuj.shah70@yahoo.com', 'anuj shah', 'xyz', 0, '1994-04-13', '42', 0, '123', '456', '789', 'anuj@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `fldt`
--

CREATE TABLE `fldt` (
  `ID` int(11) NOT NULL,
  `name_fac` varchar(50) NOT NULL,
  `academic_year` varchar(50) NOT NULL,
  `f_year` int(4) NOT NULL,
  `in_prog` int(2) NOT NULL,
  `other_prog` int(2) NOT NULL,
  `pg` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fldt_res`
--

CREATE TABLE `fldt_res` (
  `ID` int(11) NOT NULL,
  `a_c` int(4) NOT NULL,
  `b` int(4) NOT NULL,
  `f` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `internat_con`
--

CREATE TABLE `internat_con` (
  `id` int(11) NOT NULL,
  `coauthor` varchar(500) NOT NULL,
  `researchpaper` varchar(500) NOT NULL,
  `year` varchar(500) NOT NULL,
  `con_detail` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internat_con`
--

INSERT INTO `internat_con` (`id`, `coauthor`, `researchpaper`, `year`, `con_detail`) VALUES
(8, 'aa', 'aa', 'aa', 'aa'),
(9, 'ddss', 'sssdd', 'ssssssssssssdd', 'dd');

-- --------------------------------------------------------

--
-- Table structure for table `internat_jou`
--

CREATE TABLE `internat_jou` (
  `id` int(11) NOT NULL,
  `coauthor` varchar(500) NOT NULL,
  `researchpaper` varchar(500) NOT NULL,
  `year` varchar(500) NOT NULL,
  `jou_detail` varchar(500) NOT NULL,
  `impactfact` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internat_jou`
--

INSERT INTO `internat_jou` (`id`, `coauthor`, `researchpaper`, `year`, `jou_detail`, `impactfact`) VALUES
(8, 'dd', 'dd', 'dd', 'dd', 'ddd'),
(9, 'dd', 'dd', 'dd', 'dd', 'dd'),
(10, 'as1', '3qas', 'as', 'as', 'as');

-- --------------------------------------------------------

--
-- Table structure for table `metadata`
--

CREATE TABLE `metadata` (
  `key` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nat_con`
--

CREATE TABLE `nat_con` (
  `id` int(11) NOT NULL,
  `coauthor` varchar(500) NOT NULL,
  `researchpaper` varchar(500) NOT NULL,
  `year` varchar(500) NOT NULL,
  `con_detail` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nat_con`
--

INSERT INTO `nat_con` (`id`, `coauthor`, `researchpaper`, `year`, `con_detail`) VALUES
(8, 'sss', 'ss', 'ss', 'ss'),
(11, 'as', 'ax', 'as', 'as'),
(13, 'aa', 'ccvcc', 'xcc', 'cdv'),
(14, 'xyz', 'abc', 'pqr', '1223');

-- --------------------------------------------------------

--
-- Table structure for table `nat_jou`
--

CREATE TABLE `nat_jou` (
  `id` int(11) NOT NULL,
  `coauthor` varchar(500) NOT NULL,
  `researchpaper` varchar(500) NOT NULL,
  `year` varchar(500) NOT NULL,
  `jou_detail` varchar(500) NOT NULL,
  `impactfact` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nat_jou`
--

INSERT INTO `nat_jou` (`id`, `coauthor`, `researchpaper`, `year`, `jou_detail`, `impactfact`) VALUES
(3, 'deepesh', 'bbb', 'jjj', 'kk', 'jj'),
(4, 'aakash', 'shah', '2016', 'dddd', 'dd'),
(5, 'qaaa11', '2222', 'aaa', 'sss', 'ss'),
(6, 'ddd', 'ddd', 'ddd', 'dd', 'dd'),
(7, 'ddd', 'sss', 'aa', 'a', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `training_details`
--

CREATE TABLE `training_details` (
  `id` int(20) NOT NULL,
  `title` varchar(200) NOT NULL,
  `period` varchar(200) NOT NULL,
  `trainingplace` varchar(500) NOT NULL,
  `duration` varchar(200) NOT NULL,
  `points` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_details`
--

INSERT INTO `training_details` (`id`, `title`, `period`, `trainingplace`, `duration`, `points`) VALUES
(1, 'dd', 'dd', 'ddd', 'ddds', 'sss'),
(2, 'dd121', '12dd', '12dd', 'dd12', '12ddddd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `PID` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  `enroll_id` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `mobileno` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`PID`, `name`, `enroll_id`, `password`, `email`, `gender`, `mobileno`) VALUES
(1, '', '', '81dc9bdb52d04dc20036dbd8313ed055', 'anuj.shah70@yahoo.com', 0, ''),
(2, '', '', '81dc9bdb52d04dc20036dbd8313ed055', 'gfxbandits@gmail.com', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_details`
--
ALTER TABLE `academic_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `adminaccount`
--
ALTER TABLE `adminaccount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `basic_info`
--
ALTER TABLE `basic_info`
  ADD PRIMARY KEY (`PID`);

--
-- Indexes for table `fldt`
--
ALTER TABLE `fldt`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `fldt_res`
--
ALTER TABLE `fldt_res`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `internat_con`
--
ALTER TABLE `internat_con`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internat_jou`
--
ALTER TABLE `internat_jou`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nat_con`
--
ALTER TABLE `nat_con`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nat_jou`
--
ALTER TABLE `nat_jou`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_details`
--
ALTER TABLE `training_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`PID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_details`
--
ALTER TABLE `academic_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `adminaccount`
--
ALTER TABLE `adminaccount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `basic_info`
--
ALTER TABLE `basic_info`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fldt`
--
ALTER TABLE `fldt`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fldt_res`
--
ALTER TABLE `fldt_res`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `internat_con`
--
ALTER TABLE `internat_con`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `internat_jou`
--
ALTER TABLE `internat_jou`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `nat_con`
--
ALTER TABLE `nat_con`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `nat_jou`
--
ALTER TABLE `nat_jou`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `training_details`
--
ALTER TABLE `training_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
