<?php
include 'database.php';
session_start();
if(!isset($_SESSION['email'])){
header("Location:login.php");
}
$useremail=$_SESSION['email'];
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Academic Form Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href="vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="css/forms.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/jquery.js"></script>
   	 <script type="text/javascript">
			$(document).ready(function(){
				
			$("#sub").click(function(){
			//var department=$("#department").val();
			var department=$('input[name=department]:checked').val();	
			var designation=$("#designation").val();
			var adhoc=$('input[name=adhoc]:checked').val();			
			var regular=$('input[name=regular]:checked').val();

			var approvalcall=$('input[name=approvalcall]:checked').val();
			
			var callapproval=$("#callapproval").val();

			var dateregular=$("#dateregular").val();
			var dateadhoc=$("#dateadhoc").val();

			var achievement=$("#achievement").val();
			var publication=$("#publication").val();

			$.ajax({
				url:'academicinsert.php',
				data:{department:department,adhoc:adhoc,dateregular:dateregular,dateadhoc:dateadhoc,designation:designation,regular:regular,achievement:achievement,publication:publication,approvalcall:approvalcall,callapproval:callapproval},
				type:'POST',
				success:function(data)
				{
					alert(data);
				}
			
				
						});

					});

				});
		</script>
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-10">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Faculty Information And Contribution Management System</a></h1>
	              </div>
	           </div>
	          
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="profile.html">Profile</a></li>
	                          <li><a href="login.html">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <?php include 'sidenavbar.php'; ?>
		  <div class="col-md-10">

	  			<div class="row">
	  				<div class="col-md-12">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title">Academic Detail</div>
					          <div id="#result"></div>
					        
					        </div>
			  				<div class="panel-body">
			  					<form class="form-horizontal" method="post" onsubmit="return false">
			  							<div class="form-group">
											<label class="col-md-2 control-label">Department</label>
											<div class="col-md-10">
												<label class="radio radio-inline">
													<input type="radio" id="department" name="department"  value="electronics">
													Electronics </label>
												<label class="radio radio-inline">
													<input type="radio" id="department"  name="department" value="comps">
													Comps </label>
												<label class="radio radio-inline">
													<input type="radio" id="department" name="department" value="it">
													IT </label>	
												<label class="radio radio-inline">
													<input type="radio" id="department" sname="department" value="extc">
													Extc </label>	

											</div>
								  </div>
								  <div class="form-group">
								    <label for="inputEmail3" class="col-sm-2 control-label">Designation since joining</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="designation" name="designation" placeholder="Designation">
								    </div>


								  </div>
								  <div class="form-group">
											<label class="col-md-2 control-label">Date of joining on Adhoc basis</label>
											<div class="col-md-10">
												<label class="radio radio-inline">
													<input type="radio" name="adhoc" id="adhoc"  value="yes">
													Yes </label>
												<label class="radio radio-inline">
													<input type="radio" name="adhoc" id="adhoc" value="no">
													No </label>
											
											</div>
											
								</div>
										<div id="adhocdate" style="display: none" class="form-group">
											<label class="col-md-2 control-label"></label>
											<div class="col-md-10">
												<input type="date" name="dateadhoc" id="dateadhoc" class="form-control">
											</div>										
										</div>
								<div class="form-group">
											<label class="col-md-2 control-label">Date of joining on Regular basis</label>
											<div class="col-md-10">
												<label class="radio radio-inline">
													<input type="radio" name="regular" id="regular" value="yes">
													Yes </label>
												<label class="radio radio-inline">
													<input type="radio" name="regular" id="regular"  value="no">
													No </label>
											</div>
								</div>
										<div id="regulardate" style="display: none" class="form-group">
											<label class="col-md-2 control-label"></label>
											<div class="col-md-10">
												<input type="date" name="dateregular" id="dateregular" class="form-control">
											</div>										
										</div>
								<div class="form-group">
								    <label for="inputEmail3" class="col-sm-2 control-label">Approval Status</label>
								    <div class="col-md-10">
												<label class="radio radio-inline">
													<input type="radio" name="approvalcall" id="approvalcall"  value="yes">
													Yes </label>
												<label class="radio radio-inline">
													<input type="radio" name="approvalcall" id="approvalcall" value="no">
													No </label>
									</div>
								</div>	
										<div id="approvalcall2" style="display: none" class="form-group">
											<label class="col-md-2 control-label"></label>
											<div class="col-md-10">
												<input type="text" name="callapproval" id="callapproval" class="form-control" placeholder="Concall No.">
											</div>										
										</div>		
								<div class="form-group">
								    <label class="col-sm-2 control-label">Achievements</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" name="achievement" placeholder="Achievements" id="achievement">
								    </div>
								</div>
								 	
								<div class="form-group">
								    <label for="inputAge" class="col-sm-2 control-label">No. of publication</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" name="publication" placeholder="Publication" id="publication">
								    </div>
								</div>  
							
				  				<div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								     
								      <input type="submit" class="btn btn-primary" name="sub" id="sub" value="Submit">
								    </div>
								  </div>
								</form>
			  				</div>
			  			</div>
	  				</div>
	  				
	  			</div>
	  		<!--  Page content -->
		  </div>
		</div>
    </div>

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2014 <a href='#'>Website</a>
            </div>
            
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="js/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>


    <script src="vendors/form-helpers/js/bootstrap-formhelpers.min.js"></script>

    <script src="vendors/select/bootstrap-select.min.js"></script>

    <script src="vendors/tags/js/bootstrap-tags.min.js"></script>

    <script src="vendors/mask/jquery.maskedinput.min.js"></script>

    <script src="vendors/moment/moment.min.js"></script>

    <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

     <!-- bootstrap-datetimepicker -->
     <link href="vendors/bootstrap-datetimepicker/datetimepicker.css" rel="stylesheet">
     <script src="vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script> 


    <link href="http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <script src="js/custom.js"></script>
    <script src="js/forms.js"></script>
     <script type="text/javascript">
    	$(function() {
		    $('input[name="adhoc"]').on('click', function() {
		        if ($(this).val() == 'yes') {
		            $('#adhocdate').show();
		        }
		        if ($(this).val() == 'no') {
		            $('#adhocdate').hide();
		        }
		 
		    });
		
		});
    	$(function() {
			$('input[name="regular"]').on('click', function() {
			        if ($(this).val() == 'yes') {
			            $('#regulardate').show();
			        }
			        if ($(this).val() == 'no') {
		            	$('#regulardate').hide();
		        	}
			       
			    }); 
			});        
		$(function() {
			$('input[name="approvalcall"]').on('click', function() {
			        if ($(this).val() == 'yes') {
			            $('#approvalcall2').show();
			        }
			        if ($(this).val() == 'no') {
		            	$('#approvalcall2').hide();
		        	}
			     
			    });    	
		});	
    </script>

  </body>
</html>