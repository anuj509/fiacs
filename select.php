 <?php  
 include "database.php"; 
 $output = '';  
 $sql = "SELECT * FROM nat_jou";  
 $result = mysql_query($sql);  
 $output .= '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
              <thead>
                <tr>  
                     <th>Id</th>  
                     <th>Co Author</th>  
                     <th>Research Paper</th>
                     <th>Year</th>  
                     <th>Journal Details</th>
                     <th>Impact Factor</th>
                     <th>Delete</th>  
                </tr>
                </thead>
                <tbody>';  
 if(mysql_num_rows($result) > 0)  
 {    $i=1;
      while($row = mysql_fetch_array($result))  
      {  
           $output .= '  
                
                <tr>  
                     <td>'.$i.'</td>  
                     <td class="coauthor" data-id1="'.$row["id"].'" contenteditable>'.$row["1"].'</td>  
                     <td class="researchpaper" data-id2="'.$row["id"].'" contenteditable>'.$row["2"].'</td> 
                     <td class="year" data-id3="'.$row["id"].'" contenteditable>'.$row["3"].'</td>  
                     <td class="jou_detail" data-id4="'.$row["id"].'" contenteditable>'.$row["4"].'</td> 
                     <td class="impactfact" data-id5="'.$row["id"].'" contenteditable>'.$row["5"].'</td>

                     <td><button type="button" name="delete_btn" data-id3="'.$row["0"].'" class="btn btn-xs btn-danger btn_delete">x</button></td>  
                </tr>  
           ';  
      $i++; 
      } 

      $output .= '  
           <tr>  
                <td></td>  
                <td id="coauthor" contenteditable></td>  
                <td id="researchpaper" contenteditable></td>  
                <td id="year" contenteditable></td>  
                <td id="jou_detail" contenteditable></td>  
                <td id="impactfact" contenteditable></td>  
                
                <td><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success">+</button></td>  
           </tr>  
      ';  
 }  
 else  
 {  
      $output .= '<tr>  
                          <td colspan="7">Data not Found</td>  
                     </tr>';  
 }  
 $output .= '</tbody>
              </table>';  
 echo $output;  
 ?>  