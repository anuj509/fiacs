<div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li><a href="index.html"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                    <li><a href="facultyform.php"><i class="glyphicon glyphicon-calendar"></i> Personal Details</a></li>
                    <li class="submenu">
                         <a href="academicform.php">
                            <i class="glyphicon glyphicon-list"></i> Academic Details
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="#"><i class="glyphicon glyphicon-flag"></i> Achievements</a></li>
                            <li><a href="nat_con.php">Conferences</a></li>
                            <li><a href="nat_journal.php">Journals</a></li>

                        </ul>
                    </li>
                    <li><a href="training.php"><i class="glyphicon glyphicon-list"></i> Training Details</a></li>
                    
                </ul>
             </div>
		  </div>