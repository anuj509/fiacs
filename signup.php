<?php
include 'database.php';
if(isset($_POST['signup'])){
if(!empty($_POST['email']) && !empty($_POST['pass']) && !empty($_POST['cnfpass'])){
//print_r($_POST);
$email=mysql_real_escape_string($_POST['email']);
$pass=mysql_real_escape_string($_POST['pass']);
$cnfpass=mysql_real_escape_string($_POST['cnfpass']);
$res=mysql_query("SELECT * FROM `users` WHERE `email`='$email'");
if(mysql_num_rows($res)==0){
if(strcmp($pass,$cnfpass)==0){
	$pass=md5($cnfpass);
	if(mysql_query("INSERT INTO `users`(`password`, `email`) VALUES ('$pass','$email')")){
		//echo "success";
		echo '<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Registration is Successful.
</div>';
	}
		else{
			echo '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Something went Wrong!</strong>
</div>';
		}
}else{
	echo '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Password and Confirm Password does not match</strong>
</div>';
}
}else{
	//echo "user exists";
	echo '<div class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>User Already Exists!</strong>
</div>';
}
}else{

	echo '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Fields were Empty!</strong>
</div>';
}	
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Fiacs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Faculty Information And Contribution Management System</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			            <form method="post">
			                <h6>Sign Up</h6>
			                <input class="form-control" name="email" type="text" placeholder="E-mail address" required>
			                <input class="form-control" name="pass" type="password" placeholder="Password" required>
			                <input class="form-control" name="cnfpass" type="password" placeholder="Confirm Password" required>
			                <div class="action">
			                    <button class="btn btn-primary signup" name="signup" type="submit" href="index.html">Sign Up</button>
			                </div>
			             </form>                   
			            </div>
			        </div>

			        <div class="already">
			            <p>Have an account already?</p>
			            <a href="login.php">Login</a>
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>