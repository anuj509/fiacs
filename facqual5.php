<?php
session_start();
$_SESSION['year']=array('2015-2016','2014-2015','2013-2014');
$_SESSION['x']=array('1','1','0');
$_SESSION['y']=array('35','33','17');
$_SESSION['f']=array('40.8','40.8','40.8');
  $fq=array();
  
    foreach (array_keys($_SESSION['x'] + $_SESSION['y'] + $_SESSION['f'])as $key) 
    {
     
      $fq[$key]=round(2.58*(((10*$_SESSION['x'][$key])+(6*$_SESSION['y'][$key]))/$_SESSION['f'][$key]),2);
    }
 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>FLDT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link rel='stylesheet prefetch' href='https://s3-us-west-2.amazonaws.com/s.cdpn.io/123941/footable.core.css'>
     <link rel="stylesheet" href="css/normalize.css">
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="header">
       <div class="container">
          <div class="row">
             <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                   <h1><a href="index.html">FLDT</a></h1>
                </div>
             </div>
             <div class="col-md-5">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="input-group form">
                         <input type="text" class="form-control" placeholder="Search...">
                         <span class="input-group-btn">
                           <button class="btn btn-primary" type="button">Search</button>
                         </span>
                    </div>
                  </div>
                </div>
             </div>
             <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                      <ul class="nav navbar-nav">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                          <ul class="dropdown-menu animated fadeInUp">
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="login.html">Logout</a></li>
                          </ul>
                        </li>
                      </ul>
                    </nav>
                </div>
             </div>
          </div>
       </div>
  </div>

    <div class="page-content">
      <div class="row">
      <?php include 'sidenavbar.php'; ?>
      <div class="col-md-10">
      
        <div class="content-box-large">
          <div class="panel-heading">
          <div class="panel-title">Faculty Qualification</div>
        </div>
          <div class="panel-body">
      
    <table  class="display table-bordered ">
        <thead>
            <tr>
              <th>Year</th>
              <th>X</th>
              <th>Y</th>
              <th>F</th>
              <th>FQ=2.5*((10X+6Y)/F)</th>
            </tr>
            </thead>
     
        <tbody>
          <?php
          for($i = 0; $i < count($_SESSION['year']); $i++) 
          {
          ?>
            <tr>
            <td><?php echo $_SESSION['year'][$i]; ?></td>
            <td><?php echo $_SESSION['x'][$i]; ?></td>
            <td><?php echo $_SESSION['y'][$i]; ?></td>
            <td><?php echo $_SESSION['f'][$i]; ?></td>
            <td><?php echo $fq[$i]; ?></td>
            </tr>   
          <?php } ?>
           
        </tbody>
    </table>
  </div>
          </div>
        </div>



      </div>
    </div>
    

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2016 <a href='#'>Website</a>
            </div>
            
         </div>
      </footer>

    <link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/123941/footable.js'></script>
    <script src='https://cdn.jsdelivr.net/jquery.footable/2.0.3/footable.paginate.min.js'></script>

    <script src=" https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>

    <script src="vendors/datatables/dataTables.bootstrap.js"></script>

    <script src="js/custom.js"></script>
    <script src="js/tables.js"></script>
    
  </body>
</html>