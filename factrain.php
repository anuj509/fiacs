<?php
session_start();
$_SESSION['name']=array('xyz','pqr','lmn','abc','stu','lpq');
$_SESSION['year13']=array('3','3','','3','3','');
$_SESSION['year14']=array('','','','3','3','3');
$_SESSION['year15']=array('3','5','5','5','','3');
 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap Admin Theme v3</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link rel='stylesheet prefetch' href='https://s3-us-west-2.amazonaws.com/s.cdpn.io/123941/footable.core.css'>
     <link rel="stylesheet" href="css/normalize.css">
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="header">
       <div class="container">
          <div class="row">
             <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                   <h1><a href="index.html">FLDT</a></h1>
                </div>
             </div>
             <div class="col-md-5">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="input-group form">
                         <input type="text" class="form-control" placeholder="Search...">
                         <span class="input-group-btn">
                           <button class="btn btn-primary" type="button">Search</button>
                         </span>
                    </div>
                  </div>
                </div>
             </div>
             <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                      <ul class="nav navbar-nav">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                          <ul class="dropdown-menu animated fadeInUp">
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="login.html">Logout</a></li>
                          </ul>
                        </li>
                      </ul>
                    </nav>
                </div>
             </div>
          </div>
       </div>
  </div>

    <div class="page-content">
      <div class="row">
      <?php include 'sidenavbar.php'; ?>
      <div class="col-md-10">
      
        <div class="content-box-large">
          <div class="panel-heading">
          <div class="panel-title">Faculty Development</div>
        </div>
          <div class="panel-body">
      
    <table  class="display table-bordered ">
        <thead>
            <tr>
              <th rowspan="2">Srno</th>
              <th rowspan="2">Name of faculty</th>
              <th colspan="3">Max per faculty</th>
              
            </tr>
            <tr>
              <th>2013-14</th>
              <th>2014-15</th>
              <th>2015-16</th>
            </tr>
            </thead>
     
        <tbody>
          <?php
          $j=1;
          for($i = 0; $i < count($_SESSION['name']); $i++) 
          {
          ?>
            <tr>
            <td><?php echo $j; ?></td>
            <td><?php echo $_SESSION['name'][$i]; ?></td>
            <td><?php echo $_SESSION['year13'][$i]; ?></td>
            <td><?php echo $_SESSION['year14'][$i]; ?></td>
            <td><?php echo $_SESSION['year15'][$i]; ?></td>
            </tr> 
            
          <?php $j++; } ?>
           <tr>
            <td colspan="2"><b>SUM</b></td>
            <td>12</td>
            <td>9</td>
            <td>24</td>
           </tr>
           <tr>
            <td colspan="2"><b>RF=N/F</b></td>
            <td>40.8</td>
            <td>40.8</td>
            <td>40.8</td>
           </tr>
           <tr>
            <td colspan="2"><b>ASSESSMENT=3XSUM/0.5RF</b></td>
            <td>1.764</td>
            <td>1.323</td>
            <td>3.529</td>
           </tr>
           <tr>
            <td colspan="2"><b>AVERAGE</b></td>
            <td></td>
            <td></td>
            <td>2.204</td>
           </tr>
        </tbody>
    </table>
  </div>
          </div>
        </div>



      </div>
    </div>
    

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2016 <a href='#'>Website</a>
            </div>
            
         </div>
      </footer>

    <link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/123941/footable.js'></script>
    <script src='https://cdn.jsdelivr.net/jquery.footable/2.0.3/footable.paginate.min.js'></script>

    <script src=" https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>

    <script src="vendors/datatables/dataTables.bootstrap.js"></script>

    <script src="js/custom.js"></script>
    <script src="js/tables.js"></script>
    
  </body>
</html>