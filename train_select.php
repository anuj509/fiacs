 <?php  
 include "database.php"; 
 $output = '';  
 $sql = "SELECT * FROM training_details";  
 $result = mysql_query($sql);  
 $output .= '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
              <thead>
                <tr>  
                     <th>Id</th>  
                     <th>Title</th>  
                     <th>Period</th>
                     <th>Training Place</th>  
                     <th>Duration</th>
                     <th>Points</th>
                     <th>Delete</th>  
                </tr>
                </thead>
                <tbody>';  
 if(mysql_num_rows($result) > 0)  
 {    $i=1;
      while($row = mysql_fetch_array($result))  
      {  
           $output .= '  
                
                <tr>  
                     <td>'.$i.'</td>  
                     <td class="title" data-id1="'.$row["id"].'" contenteditable>'.$row["1"].'</td>  
                     <td class="period" data-id2="'.$row["id"].'" contenteditable>'.$row["2"].'</td> 
                     <td class="trainingplace" data-id3="'.$row["id"].'" contenteditable>'.$row["3"].'</td>  
                     <td class="duration" data-id4="'.$row["id"].'" contenteditable>'.$row["4"].'</td> 
                     <td class="points" data-id5="'.$row["id"].'" contenteditable>'.$row["5"].'</td>                     
                     <td><button type="button" name="delete_btn" data-id3="'.$row["0"].'" class="btn btn-xs btn-danger btn_delete">x</button></td>  
                </tr>  
           ';  
      $i++; 
      } 

      $output .= '  
           <tr>  
                <td></td>  
                <td id="title" contenteditable></td>  
                <td id="period" contenteditable></td>  
                <td id="trainingplace" contenteditable></td>  
                <td id="duration" contenteditable></td>  
                <td id="points" contenteditable></td> 
                <td><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success">+</button></td>  
           </tr>  
      ';  
 }  
 else  
 {  
      $output .= '<tr>  
                          <td colspan="7">Data not Found</td>  
                     </tr>';  
 }  
 $output .= '</tbody>
              </table>';  
 echo $output;  
 ?>  